// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require turbolinks
//= require_tree .

function getUrlParameter(url, sParam) {
  let sPageURL = url.substring(1);
  let sURLVariables = sPageURL.split('&');
  let sParameterName;
  let i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : sParameterName[1];
    }
  }
};

function buildUserList(users) {
  // Clear HTML table
  $('#users').html('');

  users.forEach(function(user) {
    htmlString = `<tr><td>${user.name}</td><td>${user.date}</td><td>${user.number}</td><td>${user.description}</td></tr>`;
    $(htmlString).appendTo('#users');
  });
}

$(document).on('turbolinks:load', function() {
  $('.sort_link').on('click', function(e) {
    e.preventDefault();
    let anchorElement = $(this);
    let currentDirection = getUrlParameter(this.search, 'direction');
    let currentSort = getUrlParameter(this.search, 'sort');
    let nextDirection = currentDirection === 'asc' ? 'desc' : 'asc';
    let newUrl = $(this).attr('href').replace(currentDirection, nextDirection);

    $.ajax({
      dataType: 'json',
      url: this.href,
      method: 'get',
      success: function(users) {
        buildUserList(users);
        anchorElement.attr('href', newUrl);
      }
    })
  });


  $('#users_search').on('submit', function(e) {
    let form = $(this);
    let searchTerm = $('#search').val();
    e.preventDefault();
    e.stopImmediatePropagation();
    $.ajax({
      url: form.attr('action'),
      dataType: 'json',
      data: { search: searchTerm },
      method: 'GET',
      success: function(users) {
        console.log(users);
        buildUserList(users);
      }
    });
  });
});

