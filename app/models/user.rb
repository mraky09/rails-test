require 'csv'

class User < ApplicationRecord
  scope :with_name, ->(search_term) {
    where(name: search_term)
  }

  def self.import_csv_file(file)
    CSV.foreach(file.path, headers: true) do |row|
      User.create!(row.to_hash)
    end
  end
end
