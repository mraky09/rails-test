class UsersController < ApplicationController
  helper_method :sort_column, :sort_direction

  def index
    @users = User.all
    @users = @users.with_name(params[:search]) unless params[:search].blank?
    @users = @users.order(sort_column + " " + sort_direction)

    respond_to do |format|
      format.html
      format.json { render json: @users }
    end
  end

  def import
    User.import_csv_file params[:file]
    redirect_to users_path, notice: "Data imported"
  rescue => ex
    redirect_to users_path, notice: "Data can't imported #{ex}"
  end

  private

  def sort_column
    User.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
