class CreatePeople < ActiveRecord::Migration[5.1]
  def change
    create_table :people do |t|
      t.string :name
      t.date :date
      t.text :description
      t.integer :number

      t.timestamps
    end
  end
end
