require 'csv'

CSV_FILE_PATH = Rails.root.join('public/test.csv')
csv_text  = File.read(CSV_FILE_PATH)
csv = CSV.parse(csv_text, headers: true)

csv.each do |row|
  Person.create!(row.to_hash)
end
