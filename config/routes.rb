Rails.application.routes.draw do
  root 'welcome#index'

  resources :users do
    collection do
      post :import
    end
  end
end
